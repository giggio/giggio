### Português

Arquiteto e desenvolvedor, agilista, escalador, provocador. Fundou e vendeu a Lambda3. Programa porque gosta. Acredita que heterarquia funciona melhor que hierarquia. Foi reconhecido Microsoft MVP há cerca de quinze anos, dos 25+ que atua no mercado. Já palestrou sobre .NET, Rust, microsserviços, JavaScript, TypeScript, Ruby, Node.js, Frontend e Backend, Agile, etc, no Brasil, e no exterior. Liderou grupos de usuários em assuntos como arquitetura de software, Docker, e .NET.

Você me encontra no Twitter em [@giovannibassi](https://twitter.com/giovannibassi).

Outras formas de me contatar podem ser encontradas em https://links.giggio.net/bio.

### English

Software architect and developer, agilist, rock climber. He develops software as a passion, and he believes self managed people and teams are more efficient and productive than outside managed ones. He was awarded as a Microsoft MVP 15+ years ago, and has 25+ years or experience developing software. He has spoken in conferences, user groups and online about .NET, Rust, microservices, JavaScript, TypeScript, Ruby, Node.js, frontend and backend development, agile development and many other topics, all around the world. He has lead some user groups in Brazil on topics such as software architecture, .NET, and Docker.

You can find me on Twitter at [@giovannibassi](https://twitter.com/giovannibassi).

Other ways to contact me can be found at https://links.giggio.net/bio.

[![Giovanni Bassi's github stats](https://github-readme-stats.vercel.app/api?username=giggio&show_icons=true&count_private=true&include_all_commits=true&theme=chartreuse-dark)](https://github.com/anuraghazra/github-readme-stats)


[![Top Backend Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=giggio&layout=compact&langs_count=6&hide=JavaScript,CoffeeScript,CSS,HTML,TypeScript&custom_title=Top%20Backend%20Languages&theme=chartreuse-dark)](https://github.com/anuraghazra/github-readme-stats)

[![Top Frontend Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=giggio&layout=compact&hide=C%23,Rust,Shell,PowerShell&custom_title=Top%20Frontend%20Languages&theme=chartreuse-dark)](https://github.com/anuraghazra/github-readme-stats)